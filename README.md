<div align="center" style="font-size: 7em">

<img src="https://img.icons8.com/emoji/192/000000/notebook-with-decorative-cover.png"/>
</div>

<div align="center">
<h1>Booking</h1>
</div>

<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_booking"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_booking&metric=alert_status" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_booking"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_booking&metric=sqale_rating" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_booking"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_booking&metric=security_rating" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_booking"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_booking&metric=reliability_rating" alt="quality gate"></a>
<a href="https://gitlab.com/sweleven/booking/-/pipelines"><img src="https://gitlab.com/sweleven/booking/badges/master/pipeline.svg" /></a>
</p>
<hr>


## Description

Microservice for managing workspaces bookings of [BlockCOVID](https://sweleven.gitlab.io/blockcovid/) built with <a href="https://nestjs.com/">nestjs</a>.

## Docs
- [Docs](https://sweleven.gitlab.io/booking)
- [Developer manual](https://sweleven.gitlab.io/blockcovid/docs/manuale-sviluppatore/backend/booking_service/)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - [Filippo Pinton](https://gitlab.com/Butterneck.com)
- Website - [https://sweleven.gitlab.io](https://sweleven.gitlab.io/)

