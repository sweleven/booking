import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { from, Observable } from 'rxjs';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { Model } from 'mongoose';

@Injectable()
export class HistoryService {
  constructor(
    @InjectModel(Booking.name) private readonly bookingModel: Model<Booking>,
  ) {}

  findAll(headers: Headers): Observable<Booking[]> {
    const userInfo = this._getUserinfo(headers);

    return this.findHistoryForUser(userInfo.email);
  }

  findOneByEmail(email: string, headers: Headers): Observable<Booking[]> {
    const userInfo = this._getUserinfo(headers);

    // If user is not internal nor admin and is requesting history of another user
    if (
      userInfo !== 'internal' &&
      !userInfo.realm_access.roles.includes('admin') &&
      userInfo.email !== email
    ) {
      return null;
    }

    return this.findHistoryForUser(email);
  }

  findOneByWorkspace(
    workspace_id: string,
    headers: Headers,
  ): Observable<Booking[]> {
    const userInfo = this._getUserinfo(headers);

    // If user is not internal nor admin
    if (
      userInfo !== 'internal' &&
      !userInfo.realm_access.roles.includes('admin')
    ) {
      return null;
    }

    return this.findHistoryForWorkspace(workspace_id);
  }

  public findHistoryForUser(email: string): Observable<Booking[]> {
    return from(
      this.bookingModel.find({ user: email }).sort('end_datetime').exec(),
    );
  }

  public findHistoryForWorkspace(workspace_id: string): Observable<Booking[]> {
    return from(
      this.bookingModel
        .find({ workspace: workspace_id })
        .sort('end_datetime')
        .exec(),
    );
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }
}
