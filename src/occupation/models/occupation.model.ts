import { ApiProperty } from '@nestjs/swagger';

export class Occupation {
  @ApiProperty()
  occupation: number;

  @ApiProperty()
  currently_free: number;

  @ApiProperty()
  currently_occupied: number;
}
