import { ApiProperty } from '@nestjs/swagger';
export class OccupationForecast {
  @ApiProperty()
  one: number;

  @ApiProperty()
  two: number;

  @ApiProperty()
  three: number;

  @ApiProperty()
  four: number;
}
