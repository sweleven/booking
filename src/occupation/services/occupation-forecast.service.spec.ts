import { Test, TestingModule } from '@nestjs/testing';
import { OccupationForecastService } from './occupation-forecast.service';

describe('OccupationForecastService', () => {
  let service: OccupationForecastService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OccupationForecastService],
    }).compile();

    service = module.get<OccupationForecastService>(OccupationForecastService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
