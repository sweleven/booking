import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Room } from 'src/bookings/models/room.model';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { FindQueryDto } from 'src/dto/find-query.dto';
import { Occupation } from '../models/occupation.model';
import { Model } from 'mongoose';
import { BookingsService } from 'src/bookings/service/bookings.service';

@Injectable()
export class OccupationService {
  constructor(
    @InjectModel(Booking.name) private readonly bookingModel: Model<Booking>,
    private readonly http: HttpService,
    private readonly bookingService: BookingsService,
  ) {}

  findAllRooms(findQueryDto: FindQueryDto): Observable<Occupation[]> {
    Logger.log(`Finding all rooms ${JSON.stringify(findQueryDto)}`);
    return null;
  }

  findOneRoom(
    id: string,
    findQueryDto: FindQueryDto,
    headers: Headers,
  ): Observable<Occupation> {
    Logger.log(`Finding room ${id}: ${JSON.stringify(findQueryDto)}`);

    let totalWorkspacesInRoom: number;

    const userinfo = this._getUserinfo(headers);

    // this.bookingService.findAll(findQueryDto, "room", id).pipe(
    //   map((bookings: Booking[]) => bookings.length),

    // );

    // Get room informations (workspaces list and workspaces count)
    return this.http.get(`http://inventory:3002/rooms/${id}`).pipe(
      // Extract data from AxiosResponse
      map(({ data }) => data),

      // Extract workspaces ids array from room object
      map((room: Room) =>
        room.workspaces.reduce((acc, cur) => acc.concat(cur._id), []),
      ),

      // Save number of workspaces in room
      tap(
        (workspacesIds: string[]) =>
          (totalWorkspacesInRoom = workspacesIds.length),
      ),

      // Find bookings within provided dates
      switchMap((workspacesIds: string[]) => {
        return this.bookingService.findWithinDate(
          workspacesIds,
          Date.now(),
          Date.now(),
          userinfo,
        );
      }),

      // TODO: Filter by actually used, not just for booked
      // switchMap((bookings: Booking[]) => {

      // })

      // Map Bookings array to its' length
      map((bookings: Booking[]) => bookings.length),

      // Build final object
      map((bookedWorkspaces: number) => {
        return {
          occupation: bookedWorkspaces / totalWorkspacesInRoom,
          currently_free: totalWorkspacesInRoom - bookedWorkspaces,
          currently_occupied: bookedWorkspaces,
        } as Occupation;
      }),
    );
  }

  // findAllWorkspaces(findQueryDto: FindQueryDto): Observable<Occupation[]> {
  //   Logger.log(`Finding all workspaces ${JSON.stringify(findQueryDto)}`);
  //   return null;
  // }

  // findOneWorkspace(id: string, findQueryDto: FindQueryDto): Observable<Occupation> {
  //   Logger.log(`Finding workspace ${id}: ${JSON.stringify(findQueryDto)}`);
  //   return null;
  // }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }
}
