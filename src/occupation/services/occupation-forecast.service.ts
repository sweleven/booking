import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Room } from 'src/bookings/models/room.model';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { PaginationQueryDto } from 'src/dto/pagination-query.dto';
import { OccupationForecast } from '../models/occupation-forecast.model';
import { Model } from 'mongoose';
import { BookingsService } from 'src/bookings/service/bookings.service';

@Injectable()
export class OccupationForecastService {
  constructor(
    @InjectModel(Booking.name) private readonly bookingModel: Model<Booking>,
    private readonly http: HttpService,
    private readonly bookingService: BookingsService,
  ) {}

  findAllRooms(
    paginationQueryDto: PaginationQueryDto,
  ): Observable<OccupationForecast[]> {
    Logger.log(`Finding all rooms ${JSON.stringify(paginationQueryDto)}`);
    return null;
  }

  findOneRoom(
    id: string,
    paginationQueryDto: PaginationQueryDto,
    headers: Headers,
  ): Observable<OccupationForecast> {
    Logger.log(`Finding room ${id}: ${JSON.stringify(paginationQueryDto)}`);

    let totalWorkspacesInRoom: number;

    const userinfo = this._getUserinfo(headers);

    // this.bookingService.findAll(paginationQueryDto, "room", id).pipe(
    //   map((bookings: Booking[]) => bookings.length),

    // );

    // Get room informations (workspaces list and workspaces count)
    return this.http.get(`http://inventory:3002/rooms/${id}`).pipe(
      // Extract data from AxiosResponse
      map(({ data }) => data),

      // Extract workspaces ids array from room object
      map((room: Room) =>
        room.workspaces.reduce((acc, cur) => acc.concat(cur._id), []),
      ),

      // Save number of workspaces in room
      tap(
        (workspacesIds: string[]) =>
          (totalWorkspacesInRoom = workspacesIds.length),
      ),

      // Find bookings within provided dates and returns total amount of booked hours
      switchMap((workspacesIds: string[]) => {
        let forecasts: Observable<number>[] = [];

        for (let i = 0; i < 4; i++) {
          const { start_datetime, end_datetime } = this._getNextDay(i + 1);

          Logger.log(start_datetime);
          Logger.log(end_datetime);

          const occupation = this.bookingService
            .findWithinDate(
              workspacesIds,
              start_datetime,
              end_datetime,
              userinfo,
            )
            .pipe(
              map((bookings: Booking[]) => {
                Logger.log(bookings);
                return bookings.reduce(
                  (acc, cur) => acc + (cur.end_datetime - cur.start_datetime),
                  0,
                );
              }),
            );

          forecasts = forecasts.concat(occupation);
        }

        return combineLatest(forecasts);
      }),

      // Calucate occupation index: percentage of booked hours in total available hours (8hr workdays)
      map((bookedHours: number[]) => {
        return bookedHours.map((cur) => cur / (8 * totalWorkspacesInRoom));
      }),

      // Build final object
      map((forecastOccupation: number[]) => {
        const [one, two, three, four] = forecastOccupation;

        return {
          one,
          two,
          three,
          four,
        } as OccupationForecast;
      }),
    );
  }

  // findAllWorkspaces(paginationQueryDto: PaginationQueryDto): Observable<OccupationForecast[]> {
  //   Logger.log(`Finding all workspaces ${JSON.stringify(paginationQueryDto)}`);
  //   return null;
  // }

  // findOneWorkspace(id: string, paginationQueryDto: PaginationQueryDto): Observable<OccupationForecast> {
  //   Logger.log(`Finding workspace ${id}: ${JSON.stringify(paginationQueryDto)}`);
  //   return null;
  // }

  /**
   * Returns the start and the end datetime of the day (today + `daysFromNow`)
   *
   * @private
   * @param {number} daysFromNow
   * @memberof OccupationForecastService
   */
  private _getNextDay(daysFromNow: number) {
    // Start target Date as current date
    const targetDay = new Date();

    // Add `daysFromNow` to today's date
    targetDay.setDate(targetDay.getDate() + daysFromNow);

    return {
      start_datetime: targetDay.setHours(0, 0, 0),
      end_datetime: targetDay.setHours(23, 59, 59),
    };
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }
}
