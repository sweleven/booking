import { Controller, Get, Param, Query, Headers } from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { PaginationQueryDto } from 'src/dto/pagination-query.dto';
import { FindOneParams } from 'src/params/find-one.params';
import { OccupationForecast } from '../models/occupation-forecast.model';
import { OccupationForecastService } from '../services/occupation-forecast.service';

@Controller('occupation/forecast')
export class OccupationForecastController {
  constructor(
    private readonly occupationForecastService: OccupationForecastService,
  ) {}

  @Get('/rooms')
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiResponse({
    status: 200,
    description: 'Current occupation metric for all rooms',
    type: [OccupationForecast],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAllByRoom(
    @Query() paginationQueryDto: PaginationQueryDto,
  ): Observable<OccupationForecast[]> {
    return this.occupationForecastService.findAllRooms(paginationQueryDto);
  }

  @Get('/rooms/:id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiResponse({
    status: 200,
    description: 'Current occupation metric for a single room',
    type: [OccupationForecast],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findOneRoom(
    @Param() { id }: FindOneParams,
    @Query() paginationQueryDto: PaginationQueryDto,
    @Headers() headers: Headers,
  ): Observable<OccupationForecast> {
    return this.occupationForecastService.findOneRoom(
      id,
      paginationQueryDto,
      headers,
    );
  }

  // @Get('/workspace')
  // @ApiQuery({
  //   name: 'start_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'end_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'limit',
  //   required: false,
  //   type: Number
  // })
  // @ApiQuery({
  //   name: 'offset',
  //   required: false,
  //   type: Number
  // })
  // @ApiResponse({
  //   status: 200,
  //   description: 'Current occupation metric for all workspaces',
  //   type: [OccupationForecast]
  // })
  // @ApiResponse({
  //   status: 400,
  //   description: 'Invalid id format provided'
  // })
  // @ApiResponse({
  //   status: 404,
  //   description: 'No record found'
  // })
  // findAllByWorkspace(@Query() paginationQueryDto: PaginationQueryDto): Observable<OccupationForecast[]> {
  //   return this.occupationForecastService.findAllWorkspaces(paginationQueryDto);
  // }

  // @Get('/workspace/:id')
  // @ApiParam({
  //   name: 'id',
  //   required: true,
  //   type: String
  // })
  // @ApiQuery({
  //   name: 'start_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'end_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'limit',
  //   required: false,
  //   type: Number
  // })
  // @ApiQuery({
  //   name: 'offset',
  //   required: false,
  //   type: Number
  // })
  // @ApiResponse({
  //   status: 200,
  //   description: 'Current occupation metric for a single workspace',
  //   type: [OccupationForecast]
  // })
  // @ApiResponse({
  //   status: 400,
  //   description: 'Invalid id format provided'
  // })
  // @ApiResponse({
  //   status: 404,
  //   description: 'No record found'
  // })
  // findOneWorkspace(@Param() { id }: FindOneParams, @Query() paginationQueryDto: PaginationQueryDto): Observable<OccupationForecast> {
  //   return this.occupationForecastService.findOneWorkspace(id, paginationQueryDto);
  // }
}
