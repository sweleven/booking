import { Test, TestingModule } from '@nestjs/testing';
import { OccupationService } from '../services/occupation.service';
import { OccupationForecastController } from './occupation-forecast.controller';

describe('OccupationController', () => {
  let controller: OccupationForecastController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OccupationForecastController],
      providers: [OccupationService],
    }).compile();

    controller = module.get<OccupationForecastController>(
      OccupationForecastController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
