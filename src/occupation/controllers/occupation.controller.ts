import { Controller, Get, Param, Query, Headers } from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { FindQueryDto } from 'src/dto/find-query.dto';
import { FindOneParams } from 'src/params/find-one.params';
import { Occupation } from '../models/occupation.model';
import { OccupationService } from '../services/occupation.service';

@Controller('occupation')
export class OccupationController {
  constructor(private readonly occupationService: OccupationService) {}

  @Get('/rooms')
  @ApiQuery({
    name: 'start_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'end_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiResponse({
    status: 200,
    description: 'Current occupation metric for all rooms',
    type: [Occupation],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAllByRoom(@Query() findQueryDto: FindQueryDto): Observable<Occupation[]> {
    return this.occupationService.findAllRooms(findQueryDto);
  }

  @Get('/rooms/:id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiQuery({
    name: 'start_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'end_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiResponse({
    status: 200,
    description: 'Current occupation metric for a single room',
    type: [Occupation],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findOneRoom(
    @Param() { id }: FindOneParams,
    @Query() findQueryDto: FindQueryDto,
    @Headers() headers: Headers,
  ): Observable<Occupation> {
    return this.occupationService.findOneRoom(id, findQueryDto, headers);
  }

  // @Get('/workspace')
  // @ApiQuery({
  //   name: 'start_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'end_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'limit',
  //   required: false,
  //   type: Number
  // })
  // @ApiQuery({
  //   name: 'offset',
  //   required: false,
  //   type: Number
  // })
  // @ApiResponse({
  //   status: 200,
  //   description: 'Current occupation metric for all workspaces',
  //   type: [Occupation]
  // })
  // @ApiResponse({
  //   status: 400,
  //   description: 'Invalid id format provided'
  // })
  // @ApiResponse({
  //   status: 404,
  //   description: 'No record found'
  // })
  // findAllByWorkspace(@Query() findQueryDto: FindQueryDto): Observable<Occupation[]> {
  //   return this.occupationService.findAllWorkspaces(findQueryDto);
  // }

  // @Get('/workspace/:id')
  // @ApiParam({
  //   name: 'id',
  //   required: true,
  //   type: String
  // })
  // @ApiQuery({
  //   name: 'start_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'end_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'limit',
  //   required: false,
  //   type: Number
  // })
  // @ApiQuery({
  //   name: 'offset',
  //   required: false,
  //   type: Number
  // })
  // @ApiResponse({
  //   status: 200,
  //   description: 'Current occupation metric for a single workspace',
  //   type: [Occupation]
  // })
  // @ApiResponse({
  //   status: 400,
  //   description: 'Invalid id format provided'
  // })
  // @ApiResponse({
  //   status: 404,
  //   description: 'No record found'
  // })
  // findOneWorkspace(@Param() { id }: FindOneParams, @Query() findQueryDto: FindQueryDto): Observable<Occupation> {
  //   return this.occupationService.findOneWorkspace(id, findQueryDto);
  // }
}
