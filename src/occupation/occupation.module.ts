import { HttpModule, Module } from '@nestjs/common';
import { OccupationService } from './services/occupation.service';
import { OccupationController } from './controllers/occupation.controller';
import { BookingsModule } from 'src/bookings/bookings.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Booking, BookingSchema } from 'src/bookings/schemas/booking.schema';
import { OccupationForecastService } from './services/occupation-forecast.service';
import { OccupationForecastController } from './controllers/occupation-forecast.controller';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: Booking.name, schema: BookingSchema }]),
    BookingsModule,
  ],
  controllers: [OccupationController, OccupationForecastController],
  providers: [OccupationService, OccupationForecastService],
})
export class OccupationModule {}
