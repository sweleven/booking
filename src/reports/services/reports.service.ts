import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { CreateReportDto } from '../dto/create-report.dto';
import { Model } from 'mongoose';
import { from } from 'rxjs';

@Injectable()
export class ReportsService {
  constructor(
    @InjectModel(Booking.name) private readonly bookingModel: Model<Booking>,
    private readonly http: HttpService,
  ) {}

  create(createReportDto: CreateReportDto) {
    Logger.log(JSON.stringify(createReportDto));

    const { users, workspaces } = createReportDto;

    let { start_date, end_date } = createReportDto;

    if (!end_date) {
      end_date = Date.now();
    } else {
      end_date = end_date.getTime();
    }

    start_date = start_date.getTime();

    return from(
      this.bookingModel
        .find({
          user: {
            $in: users,
          },
          workspace: {
            $in: workspaces,
          },
          start_datetime: {
            $gte: start_date,
            $lte: end_date,
          },
          end_datetime: {
            $gte: start_date,
            $lte: end_date,
          },
        })
        .sort('start_datetime')
        .exec(),
    );
  }
}
