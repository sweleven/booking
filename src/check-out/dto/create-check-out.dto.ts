import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateCheckOutDto {
  @ApiProperty()
  @IsString()
  rfid: string;
}
