import { HttpModule, Module } from '@nestjs/common';
import { CheckOutService } from './services/check-out.service';
import { CheckOutController } from './controllers/check-out.controller';
import { BookingsModule } from 'src/bookings/bookings.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Booking, BookingSchema } from 'src/bookings/schemas/booking.schema';

@Module({
  imports: [
    HttpModule,
    BookingsModule,
    MongooseModule.forFeature([{ name: Booking.name, schema: BookingSchema }]),
  ],
  controllers: [CheckOutController],
  providers: [CheckOutService],
})
export class CheckOutModule {}
