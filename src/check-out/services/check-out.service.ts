import { HttpService, Injectable } from '@nestjs/common';
import { from, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Workspace } from 'src/bookings/models/workspaces.model';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { BookingsService } from 'src/bookings/service/bookings.service';
import { CreateCheckOutDto } from '../dto/create-check-out.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class CheckOutService {
  constructor(
    private readonly bookingService: BookingsService,
    private readonly http: HttpService,
    @InjectModel(Booking.name) private readonly bookingModel: Model<Booking>,
  ) {}

  create(
    createCheckOutDto: CreateCheckOutDto,
    headers: Headers,
  ): Observable<Booking> {
    const userinfo = this._getUserinfo(headers);

    return this.http
      .get(`http://inventory:3002/rfid/${createCheckOutDto.rfid}`)
      .pipe(
        map((res) => res.data),
        switchMap((workspace: Workspace) => {
          return this.bookingService.findWithinDate(
            [workspace._id],
            Date.now(),
            Date.now(),
            userinfo,
          );
        }),
        switchMap((bookings: Booking[]) => {
          if (bookings === undefined || bookings.length == 0) {
            return of(null);
          } else {
            return from(this.bookingModel.findById(bookings[0])).pipe(
              switchMap((booking: Booking) => {
                booking.consumed = true;
                booking.checkout_datetime = Date.now();
                return from(booking.save());
              }),
            );
          }
        }),
      );
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }
}
