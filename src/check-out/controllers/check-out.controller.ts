import { CheckOutService } from '../services/check-out.service';
import { CreateCheckOutDto } from '../dto/create-check-out.dto';
import { Controller, Post, Body, Headers } from '@nestjs/common';

@Controller('check-out')
export class CheckOutController {
  constructor(private readonly checkOutService: CheckOutService) {}

  @Post()
  create(
    @Body() createCheckOutDto: CreateCheckOutDto,
    @Headers() headers: Headers,
  ) {
    return this.checkOutService.create(createCheckOutDto, headers);
  }
}
