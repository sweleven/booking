import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { BookingsModule } from './bookings/bookings.module';
import * as Joi from '@hapi/joi';
import { MongooseModule } from '@nestjs/mongoose';
import { OccupationModule } from './occupation/occupation.module';
import { AvailabilityModule } from './availability/availability.module';
import { CheckInModule } from './check-in/check-in.module';
import { CheckOutModule } from './check-out/check-out.module';
import { HistoryModule } from './history/history.module';
import { ReportsModule } from './reports/reports.module';
import * as amqp_plugin from '@butterneck/mongoose-amqplib-plugin';

@Module({
  imports: [
    // TODO: configure ConfigModule:
    //    - concept variables separation
    //    - improve validation
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        PORT: Joi.number().required(),
        MONGO_USER: Joi.string().required(),
        MONGO_PASSWORD: Joi.string().required(),
        MONGO_HOST: Joi.string().required(),
        MONGO_PORT: Joi.number().required(),
        MONGO_DB: Joi.string().required(),
        RABBITMQ_USER: Joi.string().required(),
        RABBITMQ_PASSWORD: Joi.string().required(),
        RABBITMQ_HOST: Joi.string().required(),
        RABBITMQ_PORT: Joi.string().required(),
        RABBITMQ_QUEUE_NAME: Joi.string().required(),
        INVENTORY_QUEUE_NAME: Joi.string().required(),
      }),
    }),
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        // Mongodb uri composition using env variables
        uri: `mongodb://${configService.get('MONGO_USER')}:${configService.get(
          'MONGO_PASSWORD',
        )}@${configService.get('MONGO_HOST')}:${configService.get(
          'MONGO_PORT',
        )}/${configService.get('MONGO_DB')}`,

        // Enabling amqp model publishing on save and remove
        connectionFactory: (connection) => {
          const rabbitmq_user = configService.get('RABBITMQ_USER');
          const rabbitmq_password = configService.get('RABBITMQ_PASSWORD');
          const rabbitmq_host = configService.get('RABBITMQ_HOST');
          const rabbitmq_port = configService.get('RABBITMQ_PORT');
          const domain_events_queue_name = configService.get(
            'DOMAIN_EVENTS_QUEUE_NAME',
          );

          // eslint-disable-next-line @typescript-eslint/no-var-requires
          connection.plugin(amqp_plugin, {
            url: `amqp://${rabbitmq_user}:${rabbitmq_password}@${rabbitmq_host}:${rabbitmq_port}`,
            queue: domain_events_queue_name,
          });
          return connection;
        },
      }),
    }),
    BookingsModule,
    OccupationModule,
    AvailabilityModule,
    CheckInModule,
    CheckOutModule,
    HistoryModule,
    ReportsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
