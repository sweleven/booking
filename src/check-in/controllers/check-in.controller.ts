import { Body, Controller, Post, Headers } from '@nestjs/common';
import { ApiBody, ApiResponse } from '@nestjs/swagger';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { CreateCheckInDto } from '../dto/create-check-in.dto';
import { CheckInService } from '../services/check-in.service';

@Controller('check-in')
export class CheckInController {
  constructor(private readonly checkInService: CheckInService) {}

  @Post()
  @ApiBody({
    required: true,
    type: CreateCheckInDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The updated booking',
    type: Booking,
  })
  create(
    @Body() createCheckInDto: CreateCheckInDto,
    @Headers() headers: Headers,
  ) {
    return this.checkInService.create(createCheckInDto, headers);
  }
}
