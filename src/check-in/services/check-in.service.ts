import { HttpService, Inject, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { from, Observable, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { Workspace } from 'src/bookings/models/workspaces.model';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { BookingsService } from 'src/bookings/service/bookings.service';
import { CreateCheckInDto } from '../dto/create-check-in.dto';
import { Model } from 'mongoose';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class CheckInService {
  constructor(
    private readonly bookingService: BookingsService,
    private readonly http: HttpService,
    @InjectModel(Booking.name) private readonly bookingModel: Model<Booking>,
    @Inject('INVENTORY_SERVICE') private inventoryClient: ClientProxy,
  ) {}

  create(
    createCheckInDto: CreateCheckInDto,
    headers: Headers,
  ): Observable<Booking> {
    const userinfo = this._getUserinfo(headers);

    return this.http
      .get(`http://inventory:3002/rfid/${createCheckInDto.rfid}`)
      .pipe(
        map((res) => res.data),
        switchMap((workspace: Workspace) => {
          return this.bookingService.findWithinDate(
            [workspace._id],
            Date.now(),
            Date.now(),
            userinfo,
          );
        }),
        switchMap((bookings: Booking[]) => {
          Logger.log(bookings);
          if (
            bookings === undefined ||
            bookings.length === 0 ||
            bookings === null
          ) {
            // TODO: failed check-in
            return of(null);
          } else {
            return from(this.bookingModel.findById(bookings[0])).pipe(
              switchMap((booking: Booking) => {
                booking.consumed = true;
                booking.checkin_datetime = Date.now();
                return from(booking.save());
              }),
            );
          }
        }),
        filter((val) => val != null),
        switchMap((booking: Booking) => {
          return this._set_workspace_dirty(booking.workspace).pipe(
            map(() => booking),
          );
        }),
      );
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }

  private _set_workspace_dirty(workspace_id: string): Observable<void> {
    return this.inventoryClient.emit('setWorkspaceCleanState', {
      workspace: workspace_id,
      state: false,
    });
  }
}
