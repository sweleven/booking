import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class Booking extends Document {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  })
  workspace: string;

  @ApiProperty()
  @Prop({
    required: true,
  })
  user: string;

  @ApiProperty()
  @Prop({
    required: true,
  })
  start_datetime: number;

  @ApiProperty()
  @Prop({
    required: true,
  })
  end_datetime: number;

  @ApiProperty()
  @Prop({
    default: () => Date.now(),
  })
  submission_datetime: number;

  @ApiProperty()
  @Prop({
    default: false,
  })
  consumed: boolean;

  @ApiProperty()
  @Prop({
    default: null,
  })
  checkout_datetime: number;

  @ApiProperty()
  @Prop({
    default: null,
  })
  checkin_datetime: number;

  @ApiProperty()
  @Prop({
    default: null,
  })
  cancellation_datetime: number;
}

export const BookingSchema = SchemaFactory.createForClass(Booking);
