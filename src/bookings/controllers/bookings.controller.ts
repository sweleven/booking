import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Logger,
  Query,
  Headers,
} from '@nestjs/common';
import { ApiBody, ApiResponse, ApiQuery, ApiParam } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { BookingsService } from '../service/bookings.service';
import { CreateBookingDto } from '../dto/create-booking.dto';
import { FindOneParams } from '../../params/find-one.params';
import { Booking } from '../schemas/booking.schema';
import { FindQueryDto } from 'src/dto/find-query.dto';

@Controller('bookings')
export class BookingsController {
  constructor(private readonly bookingsService: BookingsService) {}

  @Post()
  @ApiBody({
    required: true,
    type: CreateBookingDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The created record',
    type: Booking,
  })
  create(
    @Body() createBookingDto: CreateBookingDto,
    @Headers() headers: Headers,
  ): Observable<Booking> {
    return this.bookingsService.create(createBookingDto, headers);
  }

  // @Get()
  // @ApiQuery({
  //   name: 'limit',
  //   required: false,
  //   type: Number
  // })
  // @ApiQuery({
  //   name: 'start_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'end_datetime',
  //   required: false,
  //   type: String,
  // })
  // @ApiQuery({
  //   name: 'offset',
  //   required: false,
  //   type: Number
  // })
  // @ApiResponse({
  //   status: 200,
  //   description: 'All existing records',
  //   type: [Booking]
  // })
  // @ApiResponse({
  //   status: 400,
  //   description: 'Invalid id format provided'
  // })
  // @ApiResponse({
  //   status: 404,
  //   description: 'No record found'
  // })
  // findAll(@Query() findQueryDto: FindQueryDto): Observable<Booking[]> {
  //   Logger.debug(findQueryDto);
  //   return this.bookingsService.findAll(findQueryDto);
  // }

  @Get('')
  @ApiQuery({
    name: 'start_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'end_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'show_cancelled',
    required: false,
    type: Boolean,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [Booking],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAll(
    @Query() findQueryDto: FindQueryDto,
    @Headers() headers: Headers,
  ): Observable<Booking[]> {
    console.log(findQueryDto);
    return this.bookingsService.findAll(findQueryDto, headers, 'all');
  }

  @Get('/room/:id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiQuery({
    name: 'start_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'end_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'show_cancelled',
    required: false,
    type: Boolean,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [Booking],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAllByRoom(
    @Param() { id }: FindOneParams,
    @Query() findQueryDto: FindQueryDto,
    @Headers() headers: Headers,
  ): Observable<Booking[]> {
    Logger.debug(findQueryDto);
    return this.bookingsService.findAll(findQueryDto, headers, 'room', id);
  }

  @Get('/workspace/:id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiQuery({
    name: 'start_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'end_datetime',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'show_cancelled',
    required: false,
    type: Boolean,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [Booking],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAllByWorkspace(
    @Param() { id }: FindOneParams,
    @Query() findQueryDto: FindQueryDto,
    @Headers() headers: Headers,
  ): Observable<Booking[]> {
    Logger.debug(findQueryDto);
    return this.bookingsService.findAll(findQueryDto, headers, 'workspace', id);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Booking,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  findOne(@Param() { id }: FindOneParams): Observable<Booking> {
    return this.bookingsService.findOne(id);
  }

  // @Patch(':id')
  // @ApiParam({
  //   name: 'id',
  //   required: true,
  //   type: String
  // })
  // @ApiBody({
  //   required: true,
  //   type: UpdateBookingDto
  // })
  // @ApiResponse({
  //   status: 200,
  //   description: 'The updated record',
  //   type: Booking,
  // })
  // @ApiResponse({
  //   status: 404,
  //   description: 'No records found'
  // })
  // update(@Param() { id }: FindOneParams, @Body() updateBookingDto: UpdateBookingDto): Observable<Booking> {
  //   return this.bookingsService.update(id, updateBookingDto);
  // }

  @Delete(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The deleted Booking',
    type: Booking,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  remove(@Param() { id }: FindOneParams): Observable<Booking> {
    return this.bookingsService.remove(id);
  }
}
