import { Workspace } from './workspaces.model';

export class Room {
  _id: string;
  name: string;
  rfid: string;
  workspaces: Workspace[];
}
