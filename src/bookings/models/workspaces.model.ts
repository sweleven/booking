export class Workspace {
  _id: string;
  name: string;
  room: string;
  rfid: string;
}
