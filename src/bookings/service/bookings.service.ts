import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Observable, from, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CreateBookingDto } from '../dto/create-booking.dto';
import { Booking } from '../schemas/booking.schema';
import { Model } from 'mongoose';
import { Workspace } from '../models/workspaces.model';
import { Room } from '../models/room.model';
import { FindQueryDto } from 'src/dto/find-query.dto';

export type searchByType = 'room' | 'workspace' | 'all' | null;

@Injectable()
export class BookingsService {
  constructor(
    @InjectModel(Booking.name) private readonly bookingModel: Model<Booking>,
    private readonly http: HttpService,
  ) {}

  /**
   * Creates a Booking record in db, returns the newly created record
   *
   * @param {CreateBookingDto} createBookingDto
   * @return {*}  {Observable<Booking>}
   * @memberof BookingsService
   */
  create(
    createBookingDto: CreateBookingDto,
    headers: Headers,
  ): Observable<Booking> {
    Logger.debug(`Creating a new Booking ${createBookingDto}`);

    const { workspace, start_datetime, end_datetime } = createBookingDto;

    // Get user from jwt token
    const userinfo = this._getUserinfo(headers);

    createBookingDto['user'] = userinfo.username;

    // Check workspace availability
    return this._checkWorkspaceAvailability(
      workspace,
      start_datetime.getTime(),
      end_datetime.getTime(),
      'internal',
    ).pipe(
      switchMap((available: boolean) => {
        // If not available return notice
        if (!available) {
          // TODO: improve
          return of(null);
        } else {
          // Generating object to store in db
          const newBooking = new this.bookingModel(createBookingDto);

          return from(newBooking.save());
        }
      }),
    );
  }

  /**
   * Returns a `Booking` array containing all existing Bookings; `[]` if no Bookings are found
   *
   * @return {*}  {Observable<Booking[]>}
   * @memberof BookingsService
   */
  findAll(
    findQueryDto: FindQueryDto,
    headers: Headers,
    type: searchByType = null,
    id = '',
  ): Observable<Booking[]> {
    Logger.debug(`Finding all Bookings`);

    const userinfo = this._getUserinfo(headers);

    const { start_datetime, end_datetime, show_cancelled } = findQueryDto;

    const show_cancelled_bool = show_cancelled === 'true';

    const start_datetime_n = start_datetime ? start_datetime.getTime() : 0;
    const end_datetime_n = end_datetime
      ? end_datetime.getTime()
      : new Date().setFullYear(new Date().getFullYear() + 1);

    Logger.log(start_datetime_n);
    Logger.log(end_datetime_n);

    // Start observable chain
    let bookings = of([id]);

    // If searching by room, get all workspaces in room
    if (type === 'room') {
      bookings = bookings.pipe(
        switchMap(() => this._getWorkspacesInRoom(id)),

        // Map array of workspaces to array of workspaces ids
        map((workspaces: Workspace[]) => {
          if (!workspaces) {
            return [];
          }
          return workspaces.reduce((acc, cur) => acc.concat(cur._id), []);
        }),
      );
    }

    if (type === 'all') {
      bookings = bookings.pipe(
        switchMap(() => this._getAllWorkspaces()),

        // Map array of workspaces to array of workspaces ids
        map((workspaces: Workspace[]) => {
          if (!workspaces) {
            return [];
          }
          return workspaces.reduce((acc, cur) => acc.concat(cur._id), []);
        }),
      );
    }

    return bookings.pipe(
      switchMap((ids: string[]) =>
        this.findWithinDate(
          ids,
          start_datetime_n,
          end_datetime_n,
          userinfo,
          show_cancelled_bool,
        ),
      ),
    );

    // return this._buildFilter(type, id).pipe(
    //   switchMap((filter) => {
    //     const Bookings = this.bookingModel.find(filter).skip(offset).limit(limit).exec();

    //     return from(Bookings);
    //   }),
    // );
  }

  /**
   * Returns a `Booking`; `null` if no Bookings are found
   *
   * @param {string} id
   * @return {*}  {Observable<Booking>}
   * @memberof BookingsService
   */
  findOne(id: string): Observable<Booking> {
    Logger.debug(`Finding Booking by id`, id);
    return from(this.bookingModel.findById(id).exec());
  }

  // /**
  //  * Update Booking with id `id` (only if it already exists). Returns the
  //  * modified document
  //  *
  //  * @param {string} id
  //  * @param {UpdateBookingDto} updateBookingDto
  //  * @return {*}  {Observable<Booking>}
  //  * @memberof BookingService
  //  */
  // update(id: string, updateBookingDto: UpdateBookingDto): Observable<Booking> {
  //   Logger.debug(`Updating Booking ${id} with ${updateBookingDto}`);
  //   return from(this.BookingModel.findOneAndUpdate(
  //     { _id: id},
  //     updateBookingDto,
  //     {
  //       new: true,      // Returns modified document rather than original
  //       upsert: false,  // Does not create object if it does not exists (use @post)
  //     },
  //     )
  //   );
  // }

  /**
   * Deletes a Booking (if exists) and returns it
   *
   * @param {string} id
   * @return {*}  {Observable<Booking>}
   * @memberof BookingsService
   */
  remove(id: string): Observable<Booking> {
    Logger.debug(`Removing Booking ${id}`);
    return from(
      this.bookingModel.findOneAndUpdate(
        {
          $and: [{ _id: id }, { cancellation_datetime: null }],
        },
        {
          cancellation_datetime: Date.now(),
        },
        {
          new: true, // Returns modified document rather than original
          upsert: false, // Does not create object if it does not exists (use @post)
        },
      ),
    );
  }

  /**
   * Find all bookings on a workspace that happens between `start_datetime` and `end_datetime`
   *
   * @public
   * @param {string} workspace
   * @param {number} start_datetime
   * @param {number} end_datetime
   * @return {*}  {Observable<Booking>}
   * @memberof BookingsService
   */
  public findWithinDate(
    workspaces: string[],
    start_datetime: number,
    end_datetime: number,
    userinfo: any,
    show_cancelled = false,
  ): Observable<Booking[]> {
    // Find existing booking with overlapping start_datetime and end_datetime
    let bookings = this.bookingModel.find({
      workspace: {
        $in: workspaces,
      },
      $or: [
        // If requested start_datetime is in between and existing booking
        {
          $and: [
            {
              start_datetime: { $lte: start_datetime },
            },
            {
              end_datetime: { $gt: start_datetime },
            },
          ],
        },
        // If requested end_datetime is in between and existing booking
        {
          $and: [
            {
              start_datetime: { $lt: end_datetime },
            },
            {
              end_datetime: { $gte: end_datetime },
            },
          ],
        },
        // If requested end_datetime and start_datetime are
        {
          $and: [
            {
              start_datetime: { $gte: start_datetime },
            },
            {
              end_datetime: { $lte: end_datetime },
            },
          ],
        },
      ],
    });

    // If user is not admin select just it's booking
    if (
      userinfo !== 'internal' &&
      (!userinfo.realm_access || !userinfo.realm_access.roles.includes('admin'))
    ) {
      bookings = bookings.find({
        user: userinfo.username,
      });
    }

    if (!show_cancelled) {
      bookings = bookings.find({
        cancellation_datetime: null,
      });
    }

    return from(bookings.sort('start_datetime').exec());
  }

  /**
   * Check if the `id` room is available from `start_datetime` to `end_datetime`
   *
   * @private
   * @param {string} id
   * @param {string} start_datetime
   * @param {string} end_datetime
   * @return {*}  {Observable<boolean>}
   * @memberof BookingsService
   */
  private _checkWorkspaceAvailability(
    workspace: string,
    start_datetime: number,
    end_datetime: number,
    userinfo: any,
  ): Observable<boolean> {
    // Query for bookings between start and end datetime for room
    return this.findWithinDate(
      [workspace],
      start_datetime,
      end_datetime,
      userinfo,
    ).pipe(
      // Map presence to boolean
      map((bookings: Booking[]) => {
        Logger.log(bookings);
        return !bookings.length;
      }),
    );
  }

  /**
   *
   *
   * @private
   * @param {searchByType} type
   * @param {string} id
   * @return {*}  {Observable<any>}
   * @memberof BookingsService
   */
  private _buildFilter(type: searchByType, id: string): Observable<any> {
    let filter: Observable<any>;

    // Set filter based on type param
    switch (type) {
      case 'room':
        // Get all workspaces in room
        filter = this._getWorkspacesInRoom(id).pipe(
          // Map array of workspaces to array of workspaces ids
          map((workspaces: Workspace[]) => {
            return workspaces.reduce((acc, cur) => acc.concat(cur._id), []);
          }),

          // Build find query with workspaces ids
          map((workspacesIds: string[]) => {
            return {
              workspace: {
                $in: workspacesIds,
              },
            };
          }),
        );
        break;
      case 'workspace':
        filter = of({
          workspace: id,
        });
        break;
    }

    return filter;
  }

  /**
   *
   *
   * @private
   * @param {string} id
   * @return {*}  {Observable<Room>}
   * @memberof BookingsService
   */
  private _getRoomFromWorkspace(id: string): Observable<Room> {
    Logger.log(`_getRoomFromWorkspace ${id}`);
    return this.http.get(`http://inventory:3002/workspaces/${id}`).pipe(
      map(({ data }) => data),
      tap((data) => Logger.log(JSON.stringify(data))),
    );
  }

  /**
   *
   *
   * @private
   * @param {string} id
   * @return {*}  {Observable<Workspace[]>}
   * @memberof BookingsService
   */
  private _getWorkspacesInRoom(id: string): Observable<Workspace[]> {
    Logger.log(`_getWorkspacesInRoom ${id}`);
    return this.http.get(`http://inventory:3002/rooms/${id}`).pipe(
      map(({ data }) => data),
      map((room: Room) => {
        return room.workspaces;
      }),
    );
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }

  private _getAllWorkspaces(): Observable<Workspace[]> {
    return this.http
      .get(`http://inventory:3002/workspaces`)
      .pipe(map((resp) => resp.data));
  }
}
