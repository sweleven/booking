import { IsMongoId, IsDate } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class CreateBookingDto {
  @ApiProperty()
  @IsMongoId()
  workspace: string;

  @ApiProperty()
  @Type(() => Date)
  @IsDate()
  // @Min(() => Date.now())
  start_datetime: Date;

  @ApiProperty()
  // TODO: Use custom validator to check end_datetime > start_datetime
  @Type(() => Date)
  @IsDate()
  // @Min(this.start_datetime)
  end_datetime: Date;
}
