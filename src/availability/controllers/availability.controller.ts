import { Controller, Get, Query } from '@nestjs/common';
import { ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { Workspace } from 'src/availability/models/workspaces.model';
import { TimeIntervalQueryDto } from '../dto/time-interval-query.dto';
import { AvailabilityService } from '../services/availability.service';

@Controller('availability')
export class AvailabilityController {
  constructor(private readonly availabilityService: AvailabilityService) {}

  @Get('/workspaces')
  @ApiQuery({
    name: 'start_datetime',
    required: true,
    type: String,
  })
  @ApiQuery({
    name: 'end_datetime',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'All workspaces available between start_date and end_date',
    type: [Workspace],
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAllAvailableWorkspaces(
    @Query() timeIntervalQueryDto: TimeIntervalQueryDto,
  ): Observable<Workspace[]> {
    return this.availabilityService.findAllAvailableWorkspaces(
      timeIntervalQueryDto,
    );
  }
}
