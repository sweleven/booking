import { IsDate, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class TimeIntervalQueryDto {
  @ApiProperty()
  // Query params are string by default, casting to Date
  @Type(() => Date)
  @IsOptional()
  @IsDate()
  // @Min(() => Date.now())
  start_datetime: Date;

  @ApiProperty()
  // TODO: Use custom validator to check end_datetime > start_datetime
  @Type(() => Date)
  @IsOptional()
  @IsDate()
  // @Min(this.start_datetime)
  end_datetime: Date;
}
