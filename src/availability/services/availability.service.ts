import { HttpService, Injectable, Logger } from '@nestjs/common';
import { Booking } from 'src/bookings/schemas/booking.schema';
import { BookingsService } from 'src/bookings/service/bookings.service';
import { Observable } from 'rxjs';
import { Workspace } from 'src/bookings/models/workspaces.model';
import { TimeIntervalQueryDto } from '../dto/time-interval-query.dto';
import { map, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class AvailabilityService {
  constructor(
    private readonly http: HttpService,
    private readonly bookingService: BookingsService,
  ) {}

  findAllAvailableWorkspaces(
    timeIntervalQueryDto: TimeIntervalQueryDto,
  ): Observable<Workspace[]> {
    const { start_datetime, end_datetime } = timeIntervalQueryDto;
    Logger.debug(
      `Finding all available workspaces between ${start_datetime} and ${end_datetime}`,
    );

    let workspaces: Workspace[];

    // TODO: God save me for not using env vars
    return this.http.get(`http://inventory:3002/workspaces`).pipe(
      map(({ data }) => data),
      tap((_workspaces: Workspace[]) => (workspaces = _workspaces)),
      // Map array of workspaces to array of workspaces ids
      map((_workspaces: Workspace[]) => {
        Logger.log(`cio ${_workspaces}`);
        return _workspaces.reduce((acc, cur) => acc.concat(cur._id), []);
      }),
      // Get all bookings between start_datetime and end_datetime
      switchMap((_workspaces: string[]) => {
        return this.bookingService.findWithinDate(
          _workspaces,
          start_datetime.getTime(),
          end_datetime.getTime(),
          'internal',
        );
      }),
      // Map list of bookings to list of workspaces ids
      map((bookings: Booking[]) => {
        return bookings.reduce((acc, cur) => acc.concat(cur.workspace), []);
      }),
      // select only workspaces that are not booked
      map((_workspaces: string[]) => {
        return workspaces.filter((x) => !_workspaces.includes(x._id));
      }),
    );
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }
}
